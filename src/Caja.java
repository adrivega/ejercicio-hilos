import java.util.concurrent.Semaphore;

public class Caja implements Runnable{
    private String nombre;
    private int numeroProductos;
    private Semaphore semaphore;

    public Caja(String nombre, int numeroProductos, Semaphore semaphore) {
        this.nombre = nombre;
        this.numeroProductos = numeroProductos;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println(nombre + "esta libre");
            for (int i = 1; i <numeroProductos ; i++) {
                System.out.println(nombre + " - Cliente: Cliente " + i + " - Producto: Producto " + i);
                Thread.sleep((long)(Math.random() * 1000));

                System.out.println(nombre + "esta libre");
            }
            semaphore.release();
        } catch (InterruptedException e) {
            System.out.println("Error");
        }
    }
}
