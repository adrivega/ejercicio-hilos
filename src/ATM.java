import java.util.concurrent.Semaphore;

public class ATM extends Thread{
    public String nombre;
    public Semaphore semaphore;


    public ATM(String nombre, Semaphore semaphore) {
        this.nombre = nombre;
        this.semaphore = semaphore;
    }

    public void run(){
        try {
            semaphore.acquire();
            int tiempo = (int)(Math.random() *8) +3;
            System.out.println("ATM" + nombre + "se está utilizando en este momento");
            Thread.sleep(tiempo*10000);
            System.out.println("ATM" + nombre + "ya esta libre");
            semaphore.release();
        } catch (InterruptedException e) {
            System.out.println("Error");
        }
    }

}
