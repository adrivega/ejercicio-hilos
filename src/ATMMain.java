import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);

        ATM [] cajeros = new ATM[5];

        for (int i = 0; i < 5; i++) {
            cajeros[i] = new ATM("Cajero " + (i + 1), semaphore);
            cajeros[i].start();
        }
    }
}
