import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);

        Caja caja1 = new Caja("Caja 1", 1, semaphore);
        Caja caja2 = new Caja("Caja 2", 5, semaphore);
        Caja caja3 = new Caja("Caja 2", 2, semaphore);

        Thread hiloCaja1 = new Thread(caja1);
        Thread hiloCaja2 = new Thread(caja2);
        Thread hiloCaja3 = new Thread(caja3);

        hiloCaja1.start();
        hiloCaja2.start();
        hiloCaja3.start();
    }
}
